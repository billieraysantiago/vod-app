# React-Media-App-Vod-Accedo

Project that build a simple Media App VOD (Video On-Demand) application that keeps track of a list of videos the user has 
watched. 


***Mandatory Features
1. Load from API and display a list of videos in a scrollable horizontal carousel
on the home page. Each tile must display a movie title and an associated
cover image (look it up in the response).
2. User should be able to select a video and play it back in full screen. When
playback is finished or user quits it, user must be taken back to home page.
3. Display second “Previously watched” carousel on the home page. It must be
updated and re-sorted according to the most recently watched video.
4. The user should be able to use a mouse and keyboard (arrows/Enter keys)
to select the video.
5. Layout size adjustment. The application must be able to adjust layout
proportionally based on the desktop browser width.
***Optional Features
1. Responsive design. Change carousel to Portrait view grid if application is
run on mobile device.
2. Content list refresh button. Each click reloads content from API
3. Image caching. Upon application restart or content refresh (if implemented)
previously downloaded image is loaded from the local device cache.
4. Error handling. Note that if you simulate any errors, such as invalid movie
item, please document precisely in the accompanied Readme file.
5. Persistent storage of watched items. You can use your custom server,
database or local device cache. Note that you don’t need to store actual
movie files.
6. Unit tests. It is up to you which testing framework to use.


## Api video list example

You may use this API for a video list https://demo2697834.mockable.io/movies



## Running

Before you go through this example, you should have at least a basic understanding of ReactJS concepts. You must also already have reactjs installed on your machine.

* Test in localhost:

To run it, cd into `react-media-app-vod-accedo` and run:

```bash
npm install
npm start / yarn start
```

## Deployment

`npm run build` creates a `build` directory with a production build of your app. Set up your favourite HTTP server so that a visitor to your site is served `index.html`, and requests to static paths like `/static/js/main.<hash>.js` are served with the contents of the `/static/js/main.<hash>.js` file.

## Requirements

* [Node.js](http://nodejs.org/)
* [ReactJS](https://facebook.github.io/react/docs/installation.html)


