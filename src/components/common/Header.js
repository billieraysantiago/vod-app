import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (

    <nav className="navbar navbar-default">
			
		<div className="container">
			<div className="row">
				<div className="col-xs-6">
					<Link to="/">Home</Link>
				</div>
				<div className="col-xs-6">
					<Link to="/history">History</Link>
				</div>
			</div>
		</div>

    </nav>


);

export default Header;
