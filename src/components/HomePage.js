import React from 'react';

import MediaGalleryPage from '../container/MediaGalleryPage';

// Home page component. This serves as the welcome page.
const HomePage = () => (
    <div className="container center">
        
        <MediaGalleryPage />
    </div>
);

export default HomePage;